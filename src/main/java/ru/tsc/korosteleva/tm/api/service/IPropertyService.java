package ru.tsc.korosteleva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
