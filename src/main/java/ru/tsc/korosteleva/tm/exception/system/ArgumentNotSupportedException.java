package ru.tsc.korosteleva.tm.exception.system;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported.");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ``" + argument + "`` is not supported.");
    }
}
