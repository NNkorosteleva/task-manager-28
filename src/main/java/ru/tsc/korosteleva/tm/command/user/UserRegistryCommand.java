package ru.tsc.korosteleva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Registry new user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY NEW USER]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
