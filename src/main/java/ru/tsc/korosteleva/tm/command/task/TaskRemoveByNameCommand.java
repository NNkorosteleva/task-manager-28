package ru.tsc.korosteleva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-name";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Remove task by name.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        getTaskService().removeByName(userId, name);
    }

}
