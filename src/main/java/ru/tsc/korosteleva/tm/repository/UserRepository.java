package ru.tsc.korosteleva.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.repository.IUserRepository;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.exception.entity.UserNotFoundException;
import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.HashUtil;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserRepository(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName) {
        final User user = findOneById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User findOneByLogin(final String login) {
        return records.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return records.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User remove(final @NotNull User user) {
        final User result = Optional.ofNullable(super.remove(user))
                .orElse(null);
        final String userId = result.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return result;
    }

    @Override
    public User removeById(final @NotNull String id) {
        final User user = Optional.ofNullable(findOneById(id))
                .orElse(null);
        remove(user);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = Optional.ofNullable(findOneByLogin(login))
                .orElse(null);
        remove(user);
        return user;
    }

    @Override
    public void lockUserByLogin(String login) {
        final User user = Optional.ofNullable(findOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(String login) {
        final User user = Optional.ofNullable(findOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
    }

}
