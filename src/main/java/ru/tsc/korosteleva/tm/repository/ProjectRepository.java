package ru.tsc.korosteleva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description,
                          @NotNull final Date dateBegin,
                          @NotNull final Date dateEnd) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return add(project);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        return records.stream()
                .filter(project -> userId.equals(project.getUserId()) && name.equals(project.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project updateById(@NotNull final String userId,
                              @NotNull final String id,
                              @NotNull final String name,
                              @NotNull final String description) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project updateByIndex(@NotNull final String userId,
                                 @NotNull final Integer index,
                                 @NotNull final String name,
                                 @NotNull final String description) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        records.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusById(@NotNull final String userId,
                                           @NotNull final String id,
                                           @NotNull final Status status) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusByIndex(@NotNull final String userId,
                                              @NotNull final Integer index,
                                              @NotNull final Status status) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}