package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.repository.IProjectTaskRepository;
import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.model.Project;
import ru.tsc.korosteleva.tm.model.Task;
import ru.tsc.korosteleva.tm.service.AbstractService;
import ru.tsc.korosteleva.tm.service.AbstractUserOwnedService;

import java.util.List;

public class ProjectTaskRepository extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectTaskRepository {

    final ITaskRepository taskRepository;

    public ProjectTaskRepository(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        super(projectRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId,
                                  final String projectId,
                                  final String taskId) {
        if (!repository.existsById(userId, projectId)) return;
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId,
                                      final String projectId,
                                      final String taskId) {
        if (!repository.existsById(userId, projectId)) return;
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.stream().forEach((task) -> taskRepository.removeById(userId, task.getId()));
        repository.removeById(userId, projectId);
    }

}
